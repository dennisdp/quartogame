﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public class Field
    {
        public byte X { get; set; }

        public byte Y { get; set; }

        public Field(byte x, byte y)
        {
            X = x;
            Y = y;
        }

        public static HashSet<Field> GetAllUniqueFields(int dimension)
        {
            var fields = new HashSet<Field>();
            for (byte i = 0; i < dimension; i++)
            {
                for (byte j = 0; j < dimension; j++)
                {
                    fields.Add(new Field(i, j));
                }
            }
            return fields;
        }
        public override int GetHashCode()
        {
            int hash = 0;
            hash += Y;
            hash += X << 8;
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Field))
                return false;

            var fieldToCompare = obj as Field;
            return GetHashCode() == fieldToCompare.GetHashCode();
        }
    }
}
