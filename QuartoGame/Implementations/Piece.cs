﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public class Piece
    {
        public readonly Colour Colour;
        public readonly Hole Hole;
        public readonly Shape Shape;
        public readonly Size Size;
        public readonly byte Identifier;

        public Piece(Colour colour, Hole hole, Shape shape, Size size)
        {
            Colour = colour;
            Hole = hole;
            Shape = shape;
            Size = size;

            Identifier = GenerateIdentifier();
        }

        public static HashSet<Piece> GetAllUniquePieces()
        {
            var pieces = new HashSet<Piece>();
            for(int i = 0; i<16; i++)
            {
                var piece = new Piece((Colour)((i & 8) >> 3),(Hole)((i & 4)>>2), (Shape)((i & 2)>>1), (Size)(i & 1));
                pieces.Add(piece);
            }
            return pieces;
        }

        public override int GetHashCode()
        {
            return Identifier;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Piece))
                return false;

            var pieceToCompare = obj as Piece;
            return Identifier == pieceToCompare.Identifier;
        }

        private byte GenerateIdentifier()
        {
            int identifier = 0;

            identifier += (int)Size;
            identifier += (int)Shape << 1;
            identifier += (int)Hole << 2;
            identifier += (int)Colour << 3;

            return (byte)identifier;
        }
    }
}
