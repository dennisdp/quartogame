﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public class Board : IBoard
    {
        private readonly int _dimension;
        private readonly Piece[,] _board;

        public Board(int dimension)
        {
            _dimension = dimension;
            _board = new Piece[_dimension, _dimension];
        }

        public void SetPiece(Piece piece, Field field)
        {
                _board[field.X, field.Y] = piece;
        }

        public Piece[] GetColumn(int dimensionY)
        {
            return Enumerable.Range(0, _dimension).Select(x => _board[x, dimensionY]).ToArray();
        }

        public Piece[] GetRow(int dimensionX)
        {
            return Enumerable.Range(0, _dimension).Select(x => _board[dimensionX, x]).ToArray();
        }

        public Piece[] GetFirstDiagonal()
        {
            return Enumerable.Range(0, _dimension).Select(x => _board[x, x]).ToArray();
        }

        public Piece[] GetSecondDiagonal()
        {
            return Enumerable.Range(0, _dimension).Select(x => _board[x, _dimension - x - 1]).ToArray();
        }


    }
}
