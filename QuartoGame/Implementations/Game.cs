﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public class Game : IGame
    {
        private readonly IBoard _board;
        private GameState _gameState;

        public Game(IBoard board)
        {
            _board = board;
            _gameState = new GameState(GameStatus.Open, Piece.GetAllUniquePieces(), Field.GetAllUniqueFields(4));
        }

        public GameState Move(Piece piece, Field field)
        {
            ValidateMove(piece, field);

            _board.SetPiece(piece, field);
            _gameState.AvailablePieces.Remove(piece);
            _gameState.AvailableFields.Remove(field);

            if (IsWinningField(field))
                return new GameState(GameStatus.Win, _gameState.AvailablePieces, _gameState.AvailableFields);
            else if (_gameState.AvailablePieces.Count == 0)
                return new GameState(GameStatus.Draw, _gameState.AvailablePieces, _gameState.AvailableFields);
            else
                return new GameState(GameStatus.Open, _gameState.AvailablePieces, _gameState.AvailableFields);
        }

        private bool IsWinningField(Field field)
        {
            var linesToCheck = new List<Piece[]>();

            linesToCheck.Add(_board.GetRow(field.X));
            linesToCheck.Add(_board.GetColumn(field.Y));

            if (field.X == field.Y)
            {
                linesToCheck.Add(_board.GetFirstDiagonal());
            }
            else if (field.X + field.Y == 3)
            {
                linesToCheck.Add(_board.GetSecondDiagonal());
            }

            return linesToCheck.Any(x => IsWinningLine(x));
        }

        private bool IsWinningLine(Piece[] line)
        {
            var ids = line.Select(x => x?.Identifier).ToArray();

            if (ids.Length == 4)
            {
                var bitsUp = ids.Aggregate((x, y) => (byte)(x & y));
                var bitsDown = ids.Aggregate((x, y) => (byte)(x | y));
                return bitsUp != 0 || bitsDown != 15;
            }
            return false;
        }

        private void ValidateMove(Piece piece, Field field)
        {
            if (!_gameState.AvailablePieces.Contains(piece))
            {
                throw new ArgumentException("Piece already used.");
            }
            else if (!_gameState.AvailableFields.Contains(field))
            {
                throw new ArgumentException("Field already used.");
            }
        }
    }
}
