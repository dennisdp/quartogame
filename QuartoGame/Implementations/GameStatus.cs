﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public enum GameStatus
    {
        Open = 0,
        Draw = 1,
        Win = 2
    }
}
