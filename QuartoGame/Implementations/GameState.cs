﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public class GameState
    {
        public GameState(GameStatus gameStatus, HashSet<Piece> availablePieces, HashSet<Field> availableFields)
        {
            GameStatus = gameStatus;
            AvailablePieces = new HashSet<Piece>(availablePieces);
            AvailableFields = new HashSet<Field>(availableFields);
        }
        public GameStatus GameStatus { get; set; }

        public HashSet<Piece> AvailablePieces { get; set; }

        public HashSet<Field> AvailableFields { get; set; }
    }
}
