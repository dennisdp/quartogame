﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuartoGame
{
    public interface IBoard
    {
        void SetPiece(Piece piece, Field field);
        Piece[] GetColumn(int dimensionY);
        Piece[] GetRow(int dimensionX);
        Piece[] GetFirstDiagonal();
        Piece[] GetSecondDiagonal();
    }
}
