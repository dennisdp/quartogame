﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using QuartoGame;

namespace QuartoGame.Tests
{
    [TestFixture]
    public class Board_Tests
    {
        private List<Piece> _pieces;

        [SetUp]
        public void Setup()
        {
            _pieces = new List<Piece>
            {
                new Piece(Colour.Black, Hole.WithoutHole, Shape.Circle, Size.Big),
                new Piece(Colour.White, Hole.WithoutHole, Shape.Circle, Size.Big),
                new Piece(Colour.White, Hole.WithHole, Shape.Circle, Size.Big),
                new Piece(Colour.White, Hole.WithHole, Shape.Square, Size.Big)
            };
        }
        [Test]
        public void SetPiece_SetsPiece_WhenSlotIsFreeAndDimensionsAreValid()
        {
            var _board = new Board(4);
            _board.SetPiece(_pieces[0], new Field(0,0));
        }

        [Test]
        public void ReturnColumn_ReturnsValidColumn_WhenDimensionIsValid()
        {
            var _board = new Board(4);
            for (byte i = 0; i<4; i++)
            {
                var field = new Field(i, 0);
                _board.SetPiece(_pieces[i], field);
            }

            var column = _board.GetColumn(0);

            Assert.AreEqual(_pieces[0], column[0]);
            Assert.AreEqual(_pieces[1], column[1]);
            Assert.AreEqual(_pieces[2], column[2]);
            Assert.AreEqual(_pieces[3], column[3]);
        }

        [Test]
        public void ReturnRow_ReturnsValidRow_WhenDimensionIsValid()
        {
            var _board = new Board(4);
            for (byte i = 0; i < 4; i++)
            {
                var field = new Field(0, i);
                _board.SetPiece(_pieces[i], field);
            }

            var row = _board.GetRow(0);

            Assert.AreEqual(_pieces[0], row[0]);
            Assert.AreEqual(_pieces[1], row[1]);
            Assert.AreEqual(_pieces[2], row[2]);
            Assert.AreEqual(_pieces[3], row[3]);
        }

        [Test]
        public void ReturnFirstDiagonal_ReturnsValidFirstDiagonal()
        {
            var _board = new Board(4);
            for (byte i = 0; i < 4; i++)
            {
                var field = new Field(i, i);
                _board.SetPiece(_pieces[i], field);
            }

            var diagonal = _board.GetFirstDiagonal();

            Assert.AreEqual(_pieces[0], diagonal[0]);
            Assert.AreEqual(_pieces[1], diagonal[1]);
            Assert.AreEqual(_pieces[2], diagonal[2]);
            Assert.AreEqual(_pieces[3], diagonal[3]);
        }

        [Test]
        public void ReturnSecondDiagonal_ReturnsValidSecondDiagonal()
        {
            var _board = new Board(4);
            for (byte i = 0; i < 4; i++)
            {
                var field = new Field(i, (byte)(4 - i - 1));
                _board.SetPiece(_pieces[i], field);
            }

            var diagonal = _board.GetSecondDiagonal();

            Assert.AreEqual(_pieces[0], diagonal[0]);
            Assert.AreEqual(_pieces[1], diagonal[1]);
            Assert.AreEqual(_pieces[2], diagonal[2]);
            Assert.AreEqual(_pieces[3], diagonal[3]);
        }
    }
}
