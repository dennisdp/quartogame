﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using QuartoGame;

namespace QuartoGame.Tests
{
    [TestFixture]
    public class Piece_Tests
    {
        [Test]
        public void GeneratesValidIdentifier()
        {
            var piece1 = new Piece(Colour.Black, Hole.WithHole, Shape.Square, Size.Big);
            var piece2 = new Piece(Colour.White, Hole.WithHole, Shape.Square, Size.Big);
            var piece3 = new Piece(Colour.White, Hole.WithoutHole, Shape.Square, Size.Big);

            Assert.AreEqual(piece1.Identifier, 9);
            Assert.AreEqual(piece2.Identifier, 1);
            Assert.AreEqual(piece3.Identifier, 5);
        }
    }
}
