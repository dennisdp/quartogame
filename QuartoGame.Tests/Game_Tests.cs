﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NSubstitute;
using System.Reflection;
using QuartoGame;


namespace QuartoGame.Tests
{
    [TestFixture]
    public class Game_Tests
    {
        private HashSet<Piece> allUniquePieces;
        private HashSet<Field> allUniqueFields;

        [SetUp]
        public void SetUp()
        {
            allUniquePieces = Piece.GetAllUniquePieces();
            allUniqueFields = Field.GetAllUniqueFields(4);
        }

        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void Move_ReturnsWinGameStateWithValidPiecesAndFields_WhenValidWinningMove(int attribute)
        {
            Piece[] pieces;

            switch (attribute)
            {
                case 1:
                    pieces = allUniquePieces.Where(x => x.Colour == Colour.White).Take(4).ToArray();
                    break;
                case 2:
                    pieces = allUniquePieces.Where(x => x.Hole == Hole.WithHole).Take(4).ToArray();
                    break;
                case 3:
                    pieces = allUniquePieces.Where(x => x.Shape == Shape.Square).Take(4).ToArray();
                    break;
                case 4:
                    pieces = allUniquePieces.Where(x => x.Size == Size.Big).Take(4).ToArray();
                    break;
                default:
                    pieces = Array.Empty<Piece>();
                    break;
            };

            var board = Substitute.For<IBoard>();
            board.GetRow(0).Returns(pieces);
            board.GetColumn(default).ReturnsForAnyArgs(Array.Empty<Piece>());
            board.GetFirstDiagonal().ReturnsForAnyArgs(Array.Empty<Piece>());
            board.GetSecondDiagonal().ReturnsForAnyArgs(Array.Empty<Piece>());

            var game = new Game(board);

            var results = new List<GameState>();
            for (byte i = 0; i < 4; i++)
            {
                results.Add(game.Move(pieces[i], new Field(0, i)));
            }

            Assert.AreEqual(GameStatus.Win, results.Last().GameStatus);
            Assert.IsTrue(results.Last().AvailablePieces.Intersect(allUniquePieces).ToHashSet().Count == 12);
            Assert.IsTrue(results.Last().AvailableFields.Intersect(allUniqueFields).ToHashSet().Count == 12);
        }
        
        [Test]
        public void Move_ReturnsOpenGameStateWithValidPiecesAndFields_WhenValidNonWinningMove()
        {
            var piece = allUniquePieces.First();
            var field = new Field(0, 0);

            var board = Substitute.For<IBoard>();
            board.GetColumn(0).Returns(new Piece[] { piece });
            board.GetRow(0).Returns(new Piece[] { piece });
            board.GetFirstDiagonal().Returns(new Piece[] { piece });
            var game = new Game(board);

            var result = game.Move(piece, field);

            Assert.AreEqual(result.GameStatus, GameStatus.Open);
            Assert.IsFalse(result.AvailablePieces.Contains(piece));
            Assert.IsFalse(result.AvailableFields.Contains(field));
            Assert.IsTrue(result.AvailablePieces.Intersect(allUniquePieces).ToHashSet().Count == 15);
            Assert.IsTrue(result.AvailableFields.Intersect(allUniqueFields).ToHashSet().Count == 15);
        }
    }
}
